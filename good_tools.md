# k8s installations
EKS - https://aws.amazon.com/eks/

AKS - https://azure.microsoft.com/en-us/services/kubernetes-service/#overview

GKE - https://cloud.google.com/kubernetes-engine

Micro k8s - https://microk8s.io/

k3s - https://k3s.io/

kubeadm - https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/

docker - https://www.docker.com/products/kubernetes * Mac OS

# The kubectl cli

This allows you to interact with Kubernetes cluster(s)

kubectl - https://kubernetes.io/docs/tasks/tools/

# This is to connect to AWS/EKS clusters

aws-iam-authenticator - https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html

# The configuration to connect to each Kubernetes cluster
```
~/.kube/config
```

When you have multiple clusters this is a quick alias to add all your clusters add to your bash_profile or zshell *** may need to be modified depending on what you are doing.

This adds an easy way to switch k8s contexts(i.e. Clusters)

kubectx & kubens - https://github.com/ahmetb/kubectx

```
alias kubeconfigs='export KUBECONFIG="" && for f in `ls ~/.kube/configs/ `; do export KUBECONFIG="$HOME/.kube/configs/$f:$KUBECONFIG";  done'
```

# Helm and other tools:
https://helm.sh/docs/intro/install/ - Helm cli install

https://kompose.io/ - Allows you to create kubernetes .yaml files from your docker compose projects

https://kustomize.io/ - Is an alternative to helm, minus the templating


# Rancher & lens
https://rancher.com/ - Is an enterprise multi cluster application to allow for management and deployment of K8s clusters

https://k8slens.dev/ - Is a management multi cluster application to allow for management of your k8s clusters


# Kubernetes typical tools:

Prometheus stack -

Installs the kube-prometheus stack, a collection of Kubernetes manifests, Grafana dashboards and Prometheus rules combined with documentation and scripts to provide easy to operate end-to-end Kubernetes cluster monitoring with Prometheus using the Prometheus Operator.

https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack

# GitOps:
Flagger: Progressive Delivery Operator for Kubernetes  - https://flagger.app/

Argo: https://argo-cd.readthedocs.io/en/stable/

# Service Mesh:
Linkerd - https://linkerd.io/

Istio - https://istio.io/

# Operators:
Ingress AWS:

https://aws.amazon.com/blogs/opensource/kubernetes-ingress-aws-alb-ingress-controller/

https://github.com/kubernetes-sigs/aws-load-balancer-controller

* Note AWS has added more to this now.

# This allows you to auto create dns records for your ELB, ALB's
Route 53:

https://github.com/kubernetes-sigs/external-dns



